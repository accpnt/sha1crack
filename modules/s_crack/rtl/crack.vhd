library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;

entity crack is
	generic(
		G_CRACK_PIPELINE     : natural := 3;
		G_CRACK_HASH         : std_logic_vector(159 downto 0);
		G_KEYGEN_PREFIX_SIZE : natural := 8;
		G_KEYGEN_SUFFIX_SIZE : natural := 8;
		G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
		G_COUNTER_ORDER      : natural := 8;
		G_COUNTER_SIZE       : natural := 8
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;
		enable     : in  std_logic;
		msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
		msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
		dout       : out std_logic_vector(31 downto 0);
		ready      : out std_logic
	);
end entity crack;

-- normal xbit reverse counter (counter span from 0 to 2^x - 1)
architecture crack_arch of crack is
	-- constants

	-- components
	component keygen is
		generic (
			G_KEYGEN_PREFIX_SIZE : natural := 12;
			G_KEYGEN_SUFFIX_SIZE : natural := 12;
			G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
			G_COUNTER_ORDER      : natural := 8;
			G_COUNTER_SIZE       : natural := 8
		);
		port (
			clk        : in  std_logic;
			rst        : in  std_logic;
			enable     : in  std_logic;
			load       : in  std_logic;
			msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
			msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
			dout       : out std_logic_vector(31 downto 0); 
			ack        : out std_logic;
			last_word  : out std_logic;
			last_bytes : out std_logic_vector(1 downto 0);
			ready      : out std_logic;
			done       : out std_logic
		);
	end component keygen;
	
	component sha1 is
		port (
			clk        : in  std_logic;
			rst        : in  std_logic;  -- asynchronous reset
			start      : in  std_logic;
			load       : in  std_logic;
			last_word  : in  std_logic;
			last_bytes : in  std_logic_vector(1 downto 0);
			ack        : out std_logic;
			msg        : in  std_logic_vector( 31 downto 0);
			hash       : out std_logic_vector(159 downto 0);
			ready      : out std_logic
		);
	end component sha1;

	component comp is
		generic (
			G_CRACK_HASH : std_logic_vector(159 downto 0)
		);
		port (
			clk    : in  std_logic;
			rst    : in  std_logic;
			enable : in  std_logic;
			din    : in  std_logic_vector(159 downto 0);
			result : out std_logic;
			ready  : out std_logic
		);
	end component comp;

	component ram is
		port (
			clk           : in  std_logic;
			data          : in  std_logic_vector (31 downto 0);
			write_address : in  integer range 0 to 31;
			read_address  : in  integer range 0 to 31;
			we            : in  std_logic;
			q             : out std_logic_vector (31 downto 0)
		);
	end component ram;
	
	-- types 
	type state_type is (C_INIT, C_DELAY, C_GENERATE_KEYS, C_WAIT_FOR_HASH, C_CHECK_RESULT, C_DONE);
	type hash_type  is array (0 to G_CRACK_PIPELINE - 1) of std_logic_vector(159 downto 0);   
	type chunk_type is array (0 to G_CRACK_PIPELINE - 1) of std_logic_vector(31 downto 0);
	
	-- signals
	signal start          : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal ready_k        : std_logic := '0';
	signal enable_k       : std_logic := '0';
	signal enable_k_pulse : std_logic := '0';
	signal enable_k_q     : std_logic := '0';
	signal load_k         : std_logic := '0';
	signal load_k_q       : std_logic := '0';
	signal last_word      : std_logic := '0';
	signal last_bytes     : std_logic_vector(1 downto 0) := (others => '0');
	signal ack_k          : std_logic := '0';
	signal ack_s          : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal ready_s        : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal load_s         : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal result         : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal ready_c        : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal last_word_s    : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal write_enable   : std_logic_vector(G_CRACK_PIPELINE - 1 downto 0) := (others => '0');
	signal done           : std_logic := '0';
	signal chunk_k        : std_logic_vector(31 downto 0);
	signal chunk_s        : chunk_type := (others => (others => '0'));
	signal chunk_s0       : chunk_type := (others => (others => '0'));
	signal hash           : hash_type;
	signal state          : state_type := C_INIT;
	signal count          : natural := 0;
begin

	keygen_object : keygen
		generic map (
			G_KEYGEN_PREFIX_SIZE => G_KEYGEN_PREFIX_SIZE,
			G_KEYGEN_SUFFIX_SIZE => G_KEYGEN_SUFFIX_SIZE,
			G_COUNTER_LIMIT      => G_COUNTER_LIMIT,
			G_COUNTER_ORDER      => G_COUNTER_ORDER,
			G_COUNTER_SIZE       => G_COUNTER_SIZE
		)
		port map (
			clk        => clk,
			rst        => rst,
			enable     => enable_k,
			load       => load_k_q,
			msg_prefix => msg_prefix,
			msg_suffix => msg_suffix,
			dout       => chunk_k,
			ack        => ack_k,
			last_word  => last_word,
			last_bytes => last_bytes,
			ready      => ready_k,
			done       => done
		);
	
	SHA1N : for i in 0 to G_CRACK_PIPELINE - 1 generate
	begin
		sha1_object : sha1
			port map (
				clk        => clk,
				rst        => rst,
				start      => start(i),
				load       => load_s(i),
				last_word  => last_word_s(i), 
				last_bytes => last_bytes, 
				ack        => ack_s(i),
				msg        => chunk_s(i),
				hash       => hash(i),
				ready      => ready_s(i)
			);

		comp_object : comp 
			generic map (
				G_CRACK_HASH => G_CRACK_HASH
			)
			port map (
				clk    => clk,
				rst    => rst,
				enable => ready_s(i), 
				din    => hash(i),
				result => result(i),
				ready  => ready_c(i)
			);

		-- ram_object : ram 
		-- 	port map (
		-- 		clk           => clk,
		-- 		data          => chunk_k,
		-- 		write_address => write_address,
		-- 		read_address  => read_address,
		-- 		we            => write_enable(i),
		-- 		q             => 
		-- 	);
	end generate;

	-- combinatorial
	enable_k_pulse <= enable_k and (not enable_k_q);  
	
	keygen_load_process : process(clk, rst)
	begin
		if rst = '0' then
			load_k <= '0';
		elsif rising_edge(clk) then
			load_k <= '0';
			if enable_k_pulse = '1' or ack_s(count) = '1' then
				load_k <= '1';
			else 
				load_k <= '0';
			end if;
		end if;
	end process keygen_load_process;
	
	-- this process is in charge of loading the 
	-- current keygen output in all the sha1 
	-- objects
	sha1_load_process : process(clk, rst)
		variable i : natural := 0;
	begin
		if rst = '0' then
			load_s  <= (others => '0');
			chunk_s <= (others => (others => '0'));
		elsif rising_edge(clk) then
			-- we want load_s(count) to be a pulse
			load_s(count) <= '0';

			-- load_s(count - 1) stays high when 
			-- count changes in crack_process
			if 0 < count then 
				load_s(count - 1) <= '0';
			end if;

			-- 
			if start(count) = '1' then
				if ack_k = '1' or ready_s(count) = '1' then
					if 0 <= i and i < 3 - 1 then
						load_s(count)      <= '1';
						chunk_s(count) <= chunk_k;
						i           := i + 1;
					elsif i = 3 - 1 then 
						load_s(count)      <= '1';
						chunk_s(count) <= chunk_k;
						i           := i + 1;
					else
						load_s(count) <= '0';
					end if;
				else
					load_s(count) <= '0';
				end if;
			else 
				load_s(count) <= '0';
				i      := 0;
			end if;
		end if;
	end process sha1_load_process;
	
	-- main process, handles the state machine
	crack_process : process(clk, rst)
	begin
		if rst = '0' then
			ready <= '0';
			dout  <= (others => '0');
			state <= C_INIT;
		elsif rising_edge(clk) then
			if enable = '1'  then
				-- pulse detectors delay's
				load_k_q   <= load_k;
				enable_k_q <= enable_k;
				
				-- 
				last_word_s(count) <= last_word;
			
				-- state machine
				case state is
				when C_INIT =>
					count     <= 0;
					state     <= C_DELAY;
				when C_DELAY =>
					if and_reduce(ready_s) = '1' and ready_k = '1' then 
						state <= C_GENERATE_KEYS;
					else
						state <= C_DELAY;
					end if;
				when C_GENERATE_KEYS =>
					-- we want the keygen to be enabled only when 
					-- it is ready (reminder : keygen has a pulse 
					-- detector) and not when all sha1 instances 
					-- have been started
					if ready_k = '1' and and_reduce(start) = '0' then
						enable_k <= '1';
					else
						enable_k <= '0';
					end if;
				
					-- 
					if ack_k = '1' then
						start(count)   <= '1';
						if ready_k = '1' then 
							-- generate as many keys as pipeline depth
							if count < G_CRACK_PIPELINE - 1 then
								count <= count + 1;
								state <= C_GENERATE_KEYS;
							else
								state <= C_WAIT_FOR_HASH;
							end if;
						end if;
					end if;
				when C_WAIT_FOR_HASH =>
					start  <= (others => '0');
					-- wait for last pipeline object to be ready
					if and_reduce(ready_s) = '1' then
						state <= C_CHECK_RESULT;
						count <= 0;
					else
						state <= C_WAIT_FOR_HASH;
					end if;	
				when C_CHECK_RESULT =>
					if ready_c(G_CRACK_PIPELINE - 1) = '1' then
						if or_reduce(result) = '1' then
							for i in 0 to G_CRACK_PIPELINE - 1 loop
								if result(i) = '1' then 
									dout <= chunk_s0(i);
								end if;
							end loop;
							state <= C_DONE;
						elsif done = '1' then
							state <= C_DONE;
						else
							state    <= C_DELAY;
							chunk_s0 <= chunk_s;
						end if;
					else
						if count < G_CRACK_PIPELINE - 1 then
							count <= count + 1;
						end if;
						state <= C_CHECK_RESULT;
					end if;
				when C_DONE =>
					ready <= '1';
				end case;
			end if;
		end if;	
	end process crack_process;
end crack_arch;