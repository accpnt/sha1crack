#!/bin/sh

# create library
vlib work
vmap work work

# compile RTL files
vcom -93 -work work ../../u_comp/rtl/comp.vhd
vcom -93 -work work ../../u_keygen/rtl/xbit_counter.vhd
vcom -93 -work work ../../u_keygen/rtl/keygen.vhd
vcom -93 -work work ../../u_sha1/rtl/sha1_chunk.vhd
vcom -93 -work work ../../u_sha1/rtl/sha1_chunk_prep.vhd
vcom -93 -work work ../../u_sha1/rtl/sha1.vhd
vcom -93 -work work ../rtl/crack.vhd

# compile testbench
vcom -93 -work work ../tst/crack_test.vhd

# launch ModelSim 
vsim -t 1ps crack_test -do ./crack.do
