library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity crack_test is
end entity crack_test;

architecture crack_test_arch of crack_test is
	-- constants
	constant C_CRACK_PIPELINE     : natural := 2;
	-- 00:00:00:00:00:00:00:00:00:00:01:02:03:04:05:06:35:26:84:04:70:99:98:00
	constant C_CRACK_HASH         : std_logic_vector := X"9eceab530b9a1ca44f7d5804e856431caa3b0257";
	constant C_KEYGEN_PREFIX      : std_logic_vector := X"aaaaaaaa";
	constant C_KEYGEN_PREFIX_SIZE : natural := 32;
	constant C_KEYGEN_SUFFIX      : std_logic_vector := X"bbbbbbbb";
	constant C_KEYGEN_SUFFIX_SIZE : natural := 32;
	constant C_COUNTER_ORDER      : natural := 4;
	constant C_COUNTER_SIZE       : natural := 8;
	constant C_COUNTER_LIMIT      : std_logic_vector := "00001001";

	-- components
	component crack is
		generic(
			G_CRACK_PIPELINE     : natural := 3;
			G_CRACK_HASH         : std_logic_vector(159 downto 0);
			G_KEYGEN_PREFIX_SIZE : natural := 8;
			G_KEYGEN_SUFFIX_SIZE : natural := 8;
			G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
			G_COUNTER_ORDER      : natural := 8;
			G_COUNTER_SIZE       : natural := 8
		);
		port(
			clk        : in  std_logic;
			rst        : in  std_logic;
			enable     : in  std_logic;
			msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
			msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
			dout       : out std_logic_vector(31 downto 0);
			ready      : out std_logic
		);
	end component crack;

	-- signals 
	signal s_clk    : std_logic := '0';
	signal s_rst    : std_logic := '0';
	signal s_enable : std_logic := '0';
	signal s_dout   : std_logic_vector(31 downto 0);
	signal s_ready  : std_logic := '0';
	
	for crack_object : crack use entity work.crack(crack_arch);

begin
	crack_object : crack
		generic map (
			G_CRACK_PIPELINE     => C_CRACK_PIPELINE,
			G_CRACK_HASH         => C_CRACK_HASH,
			G_KEYGEN_PREFIX_SIZE => C_KEYGEN_PREFIX_SIZE,
			G_KEYGEN_SUFFIX_SIZE => C_KEYGEN_SUFFIX_SIZE,
			G_COUNTER_LIMIT      => C_COUNTER_LIMIT,  
			G_COUNTER_ORDER      => C_COUNTER_ORDER,
			G_COUNTER_SIZE       => C_COUNTER_SIZE
		)
		port map (
			s_clk, s_rst, s_enable, C_KEYGEN_PREFIX, C_KEYGEN_SUFFIX, s_dout, s_ready
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 1100 ns;
		s_enable <= '1';
		
		wait;
	end process sim_process;
end crack_test_arch;