# u_sha1

## Overview

This opensource SHA-1 core is written in VHDL. It supports message pre-processing. 

## Design 

The core is designed as follow : 

![Link to the design architecture diagram](https://gitlab.com/accpnt/sha1crack/raw/master/modules/u_sha1/doc/u_sha1.svg)