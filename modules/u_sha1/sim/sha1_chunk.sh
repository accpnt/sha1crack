#!/bin/sh

# create library
vlib work
vmap work work

# compile RTL files
vcom -93 -work work ../rtl/sha1_chunk.vhd

# compile testbench
vcom -93 -work work ../tst/sha1_chunk_test.vhd

# launch ModelSim 
vsim -t 1ps sha1_chunk_test -do ./sha1_chunk.do

