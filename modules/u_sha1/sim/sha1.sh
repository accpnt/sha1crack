#!/bin/sh

# create library
vlib work
vmap work work

# compile RTL files
vcom -93 -work work ../rtl/sha1_chunk.vhd
vcom -93 -work work ../rtl/sha1_chunk_prep.vhd
vcom -93 -work work ../rtl/sha1.vhd

# compile testbench
vcom -93 -work work ../tst/sha1_test.vhd

# launch ModelSim 
vsim -t 1ps sha1_test -do ./sha1.do

