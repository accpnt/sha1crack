onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/clk
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/rst
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/load
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/last_word
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/last_bytes
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ack
add wave -noupdate -expand -group sha1 -radix hexadecimal /sha1_test/sha1_object/msg
add wave -noupdate -expand -group sha1 -radix hexadecimal /sha1_test/sha1_object/hash
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ready
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/load_prep
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ack_prep
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ready_prep
add wave -noupdate -expand -group sha1 -radix hexadecimal /sha1_test/sha1_object/msg_prep
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/start
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/load_chunk
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ack_chunk
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/ready_chunk
add wave -noupdate -expand -group sha1 /sha1_test/sha1_object/state
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/clk
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/rst
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/load
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/last_word
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/last_bytes
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/ack
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/state
add wave -noupdate -expand -group prep -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_prep_object/din
add wave -noupdate -expand -group prep -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_prep_object/dout
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/ready
add wave -noupdate -expand -group prep -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_prep_object/size
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/i
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/bits
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/load_q
add wave -noupdate -expand -group prep /sha1_test/sha1_object/sha1_chunk_prep_object/load_pulse
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/clk
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/rst
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/start
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/load
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/ack
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/state
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/msg
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/hash
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/ready
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/round
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/w_round
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/w_round_temp
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/w
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/hash_round
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/a
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/b
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/c
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/d
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/e
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/a_rol
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/f
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/k
add wave -noupdate -expand -group chunk -radix hexadecimal /sha1_test/sha1_object/sha1_chunk_object/t
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/ready_extend
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/ready_round
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/ready_round_q1
add wave -noupdate -expand -group chunk /sha1_test/sha1_object/sha1_chunk_object/process_round
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7132386 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {10825943 ps}
