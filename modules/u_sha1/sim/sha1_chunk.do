onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/clk
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/rst
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/start
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/load
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/ack
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/msg
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/hash
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/ready
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/round
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/w_round
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/w_round_temp
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/w
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/hash_round
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/a
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/b
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/c
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/d
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/e
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/a_rol
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/f
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/k
add wave -noupdate -radix hexadecimal /sha1_chunk_test/sha1_chunk_object/t
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/ready_extend
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/ready_round
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/ready_round_q1
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/process_round
add wave -noupdate /sha1_chunk_test/sha1_chunk_object/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1050000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1806173535 ps}
