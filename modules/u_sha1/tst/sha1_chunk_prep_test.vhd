library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity sha1_chunk_prep_test is
end entity sha1_chunk_prep_test;

architecture sha1_chunk_prep_test_arch of sha1_chunk_prep_test is
	-- components
	component sha1_chunk_prep 
		port(
			clk        : in  std_logic;
			rst        : in  std_logic;  -- asynchronous reset
			load       : in  std_logic;
			last_word  : in  std_logic;
			last_bytes : in  std_logic_vector(1 downto 0);
			ack        : out std_logic;
			din        : in  std_logic_vector(31 downto 0) := (others => '0');  -- input message
			dout       : out std_logic_vector(0 to 31);                         -- output message
			ready      : out std_logic
		);
	end component;

	-- signals
	signal s_clk        : std_logic := '0';
	signal s_rst        : std_logic := '0';
	signal s_load       : std_logic := '0';
	signal s_last_word  : std_logic := '0';
	signal s_last_bytes : std_logic_vector(1 downto 0) := (others => '0');
	signal s_ack        : std_logic := '0';
	signal s_din        : std_logic_vector(31 downto 0) := (others => '0');
	signal s_dout       : std_logic_vector(31 downto 0) := (others => '0');
	signal s_ready      : std_logic := '0';

	for sha1_chunk_prep_object : sha1_chunk_prep use entity work.sha1_chunk_prep(sha1_chunk_prep_arch);

begin
	sha1_chunk_prep_object : sha1_chunk_prep 
		port map (
			s_clk, s_rst, s_load, s_last_word, s_last_bytes, s_ack, s_din, s_dout, s_ready
		);

	rst_process :process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;
  
	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 10 ns;

		s_din(23 downto 0)  <= X"626364";
		s_din(31 downto 24) <= (others => '0');
		s_load              <= '1';
		s_last_word         <= '1';
		s_last_bytes        <= "11";
		wait until s_ready = '1';
		s_load <= '0';
		
		wait;
		
	end process sim_process;
end sha1_chunk_prep_test_arch;
