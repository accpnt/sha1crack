library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity sha1_chunk_test is
end entity sha1_chunk_test;

architecture sha1_chunk_test_arch of sha1_chunk_test is
	-- components
	component sha1_chunk 
		port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			load  : in  std_logic;
			ack   : out std_logic;
			msg   : in  std_logic_vector( 31 downto 0);
			hash  : out std_logic_vector(159 downto 0);
			ready : out std_logic
		);
	end component;

	-- types
	type chunk is array (0 to 15) of std_logic_vector(31 downto 0); 
	
	-- signals
	signal s_clk         : std_logic := '0';
	signal s_rst         : std_logic := '0';
	signal s_load        : std_logic := '0';
	signal s_ack         : std_logic := '0';
	signal s_start       : std_logic := '0';
	signal s_msg         : std_logic_vector(31 downto 0) := (others => '0');
	signal s_chunk       : chunk := (others => (others => '0'));
	signal s_hash        : std_logic_vector(159 downto 0) := (others => 'Z');
	signal s_ready       : std_logic := '0';
	signal s_ready_pulse : std_logic := '0';
	signal s_ready_q     : std_logic := '0';
	signal s_count       : natural := 0;
	signal i             : natural := 0;

	for sha1_chunk_object : sha1_chunk use entity work.sha1_chunk(sha1_chunk_arch);

begin
	sha1_chunk_object : sha1_chunk 
		port map (
			s_clk, s_rst, s_load, s_ack, s_msg, s_hash, s_ready
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	s_ready_pulse <= s_ready and (not s_ready_q);

	-- enable process : basically a pulse detector
	ready_pulse_process : process(s_clk, s_rst)
	begin
		if s_rst = '0' then
			s_ready_q <= '0';
		elsif rising_edge(s_clk) then
			s_ready_q <= s_ready;
		end if;
	end process ready_pulse_process;    
	
	load_process : process(s_clk)
	begin
		if rising_edge(s_clk) then
			s_load <= '0';
			if s_start = '1' then
				if s_ack = '1' or s_ready_pulse = '1' then
					if 0 <= i and i < s_count  then
						s_load      <= '1';
						s_msg       <= s_chunk(i);
						i <= i + 1;
					else
						s_load <= '0';
					end if;
				else
					s_load <= '0';
				end if;
			else 
				s_load <= '0';
				i      <= 0;
			end if;
		end if;
	end process load_process;
	
	sim_process : process
	begin
		wait for 200 ns;
		-- SHA1("abc") = A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D 
		s_chunk(0)  <= X"61626380";      -- W[0]
		s_chunk(1)  <= (others => '0');  -- W[1]
		s_chunk(2)  <= (others => '0');  -- W[2]
		s_chunk(3)  <= (others => '0');  -- W[3]
		s_chunk(4)  <= (others => '0');  -- W[4]
		s_chunk(5)  <= (others => '0');  -- W[5]
		s_chunk(6)  <= (others => '0');  -- W[6]
		s_chunk(7)  <= (others => '0');  -- W[7]
		s_chunk(8)  <= (others => '0');  -- W[8]
		s_chunk(9)  <= (others => '0');  -- W[9]
		s_chunk(10) <= (others => '0');  -- W[10]
		s_chunk(11) <= (others => '0');  -- W[11]
		s_chunk(12) <= (others => '0');  -- W[12]
		s_chunk(13) <= (others => '0');  -- W[13]
		s_chunk(14) <= (others => '0');  -- W[14]
		s_chunk(15) <= X"00000018";      -- W[15]
		s_count <= 16;
		s_start <= '1';
		wait until s_ready = '1'; 
		s_start <= '0';

		wait until s_clk = '1';         
		-- SHA1("") = da39a3ee 5e6b4b0d 3255bfef 95601890 afd80709
		s_chunk(0)  <= X"80000000";      -- W[0]
		s_chunk(1)  <= (others => '0');  -- W[1]
		s_chunk(2)  <= (others => '0');  -- W[2]
		s_chunk(3)  <= (others => '0');  -- W[3]
		s_chunk(4)  <= (others => '0');  -- W[4]
		s_chunk(5)  <= (others => '0');  -- W[5]
		s_chunk(6)  <= (others => '0');  -- W[6]
		s_chunk(7)  <= (others => '0');  -- W[7]
		s_chunk(8)  <= (others => '0');  -- W[8]
		s_chunk(9)  <= (others => '0');  -- W[9]
		s_chunk(10) <= (others => '0');  -- W[10]
		s_chunk(11) <= (others => '0');  -- W[11]
		s_chunk(12) <= (others => '0');  -- W[12]
		s_chunk(13) <= (others => '0');  -- W[13]
		s_chunk(14) <= (others => '0');  -- W[14]
		s_chunk(15) <= (others => '0');  -- W[15]
		s_count <= 16;
		s_start <= '1';
		wait until s_ready = '1'; 
		s_start <= '0';

		wait until s_clk = '1';         
		-- SHA1("abcdefghijklmnopqrstuvwx") = d717e22e 1659305f ad6ef088 64923db6 4aba9c08
		s_chunk(0)  <= X"61626364";      -- W[0]
		s_chunk(1)  <= X"65666768";      -- W[1]
		s_chunk(2)  <= X"696a6b6c";      -- W[2]
		s_chunk(3)  <= X"6d6e6f70";      -- W[3]
		s_chunk(4)  <= X"71727374";      -- W[4]
		s_chunk(5)  <= X"75767778";      -- W[5]
		s_chunk(6)  <= X"80000000";      -- W[6]
		s_chunk(7)  <= (others => '0');  -- W[7]
		s_chunk(8)  <= (others => '0');  -- W[8]
		s_chunk(9)  <= (others => '0');  -- W[9]
		s_chunk(10) <= (others => '0');  -- W[10]
		s_chunk(11) <= (others => '0');  -- W[11]
		s_chunk(12) <= (others => '0');  -- W[12]
		s_chunk(13) <= (others => '0');  -- W[13]
		s_chunk(14) <= (others => '0');  -- W[14]
		s_chunk(15) <= X"000000C0";      -- W[15]
		s_count <= 16;
		s_start <= '1';
		wait until s_ready = '1'; 
		s_start <= '0';
		
		wait;
		
	end process sim_process;
end sha1_chunk_test_arch;
