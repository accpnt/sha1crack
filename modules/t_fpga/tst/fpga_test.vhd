library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fpga_test is
end entity fpga_test;

architecture fpga_test_arch of fpga_test is
	-- constants
	constant C_FPGA_VER : std_logic_vector(7 downto 0) := X"01";

	-- components
	component fpga
		generic(
			FPGA_VER : std_logic_vector(7 downto 0)
		);
		port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			start : in  std_logic;
			ready : out std_logic
		);
	end component;

	-- signals 
	signal s_clk   : std_logic := '0';
	signal s_rst   : std_logic := '0';
	signal s_start : std_logic := '0';
	signal s_ready : std_logic := '0';
	
	for fpga_object : fpga use entity work.fpga(fpga_arch);

begin
	fpga_object : fpga
		generic map (
			FPGA_VER => C_FPGA_VER
		)
		port map (
			s_clk, s_rst, s_start, s_ready
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 1000 ns;
		s_start <= '1';
		wait for 1000 ns;
		s_start <= '0';
		wait;

	end process sim_process;
end fpga_test_arch;