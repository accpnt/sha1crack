library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity io_fpga is
	generic(
		FPGA_VER : std_logic_vector(7 downto 0) := X"01"
	);
	port(
		CLOCK_50 : in  std_logic;
		RST      : in  std_logic;
		START    : in  std_logic;
		LED0     : out std_logic;
		LED1     : out std_logic;
		LED2     : out std_logic
	);
end entity io_fpga;

architecture io_fpga_arch of io_fpga is
	-- constants
	constant C_FPGA_VER : std_logic_vector(7 downto 0) := X"02";
	
	-- components
	component fpga is
	generic(
		FPGA_VER : std_logic_vector(7 downto 0)
	);
	port(
		clk   : in  std_logic;
		rst   : in  std_logic;
		start : in std_logic;
		ready : out std_logic
	);
	end component;
	
	-- signals
	signal rst_n   : std_logic;
	signal start_n : std_logic;
	
begin
	-- combinatorial
	rst_n   <= not RST;
	start_n <= not START; 
	LED1    <= rst_n;
	LED2    <= start_n;

	fpga_object : fpga
		generic map (
			FPGA_VER => C_FPGA_VER
		)
		port map (
			clk   => CLOCK_50, 
			rst   => RST,
			start => start_n,
			ready => LED0
		);
		
end io_fpga_arch;
