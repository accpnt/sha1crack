library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity fpga is
	generic(
		FPGA_VER : std_logic_vector(7 downto 0)
	);
	port(
		clk   : in  std_logic;
		rst   : in  std_logic;
		start : in  std_logic;
		ready : out std_logic
	);
end entity fpga;

architecture fpga_arch of fpga is
	-- constants
	constant C_CRACK_PIPELINE     : natural := 12;
	-- 01:02:03:04:05:06:07:08:09:08:07:06:05:04:03:00:35:26:84:04:70:99:98:00
	constant C_CRACK_HASH         : std_logic_vector := X"d9bc9f6909100fb836fa89fba62d0e43d9c50dea";  -- 05:03:06:03:06 (length=5)
	constant C_KEYGEN_PREFIX      : std_logic_vector := "";
	constant C_KEYGEN_PREFIX_SIZE : natural := 0;
	constant C_KEYGEN_SUFFIX      : std_logic_vector := X"003526840470999800";
	constant C_KEYGEN_SUFFIX_SIZE : natural := 72;
	constant C_COUNTER_ORDER      : natural := 15;
	constant C_COUNTER_SIZE       : natural := 8;
	constant C_COUNTER_LIMIT      : std_logic_vector := "00001001";
	
	-- components
	component crack is
    generic(
		G_CRACK_PIPELINE     : natural := 3;
		G_CRACK_HASH         : std_logic_vector(159 downto 0);
		G_KEYGEN_PREFIX_SIZE : natural := 8;
		G_KEYGEN_SUFFIX_SIZE : natural := 8;
		G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
		G_COUNTER_ORDER      : natural := 8;
		G_COUNTER_SIZE       : natural := 8
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;
		enable     : in  std_logic;
		msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
		msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
		dout       : out std_logic_vector(31 downto 0);
		ready      : out std_logic
	);
	end component;
	
	-- signals
	signal enable : std_logic := '0';
	signal dout   : std_logic_vector(31 downto 0) := (others => '0');
begin
	crack_object : crack
		generic map (
			G_CRACK_PIPELINE     => C_CRACK_PIPELINE,
			G_CRACK_HASH         => C_CRACK_HASH,
			G_KEYGEN_PREFIX_SIZE => C_KEYGEN_PREFIX_SIZE,
			G_KEYGEN_SUFFIX_SIZE => C_KEYGEN_SUFFIX_SIZE,
			G_COUNTER_LIMIT      => C_COUNTER_LIMIT,  
			G_COUNTER_ORDER      => C_COUNTER_ORDER,
			G_COUNTER_SIZE       => C_COUNTER_SIZE  
		)
		port map (clk, rst, enable, C_KEYGEN_PREFIX, C_KEYGEN_SUFFIX, dout, ready);

	fpga_process : process(clk, rst)
	begin
		if rst = '0' then
			enable <= '0';
		elsif rising_edge(clk) then
			if start = '1' then
				enable <= '1';
			end if ;
		end if;
	end process fpga_process;
end fpga_arch;
