library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity xbit_counter_test is
end entity xbit_counter_test;

architecture xbit_counter_test_arch of xbit_counter_test is
	constant C_COUNTER_SIZE  : natural := 7;
	constant C_COUNTER_LIMIT : std_logic_vector := "0001111";

	component xbit_counter
		generic(
			G_COUNTER_LIMIT : std_logic_vector := "0001111";
			G_COUNTER_SIZE  : natural := 8
		);
		port(
			clk         : in  std_logic;
			rst         : in  std_logic;
			enable      : in  std_logic;
			count       : in  std_logic;
			dout        : out std_logic_vector(G_COUNTER_SIZE - 1 downto 0);
			max_reached : out std_logic
		);
	end component;

	signal s_clk         : std_logic := '0';
	signal s_rst         : std_logic := '0';
	signal s_enable      : std_logic := '0';
	signal s_count       : std_logic := '0';
	signal s_dout        : std_logic_vector(C_COUNTER_SIZE - 1 downto 0) := (others => 'Z');
	signal s_max_reached : std_logic := '0';
	
	for xbit_counter_object : xbit_counter use entity work.xbit_counter(xbit_counter_arch);

begin
	xbit_counter_object : xbit_counter 
		generic map (G_COUNTER_LIMIT => C_COUNTER_LIMIT, G_COUNTER_SIZE => C_COUNTER_SIZE)
		port map (
			s_clk, s_rst, s_enable, s_count, s_dout, s_max_reached
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 10 ns;
		s_count  <= '1';
		s_enable <= '1';

		wait;
        
	end process sim_process;
end xbit_counter_test_arch;
