library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity keygen_test is
end entity keygen_test;

architecture keygen_test_arch of keygen_test is
	-- constants
	constant C_KEYGEN_PREFIX      : std_logic_vector := X"aaaaaaaa";
	constant C_KEYGEN_PREFIX_SIZE : natural := 32;
	constant C_KEYGEN_SUFFIX      : std_logic_vector := X"bb";
	constant C_KEYGEN_SUFFIX_SIZE : natural := 8;
	constant C_COUNTER_ORDER      : natural := 4;
	constant C_COUNTER_SIZE       : natural := 8;
	constant C_COUNTER_LIMIT      : std_logic_vector := "00001001";

	-- components
	component keygen
		generic(
			G_KEYGEN_PREFIX_SIZE : natural := 12;
			G_KEYGEN_SUFFIX_SIZE : natural := 12;
			G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
			G_COUNTER_ORDER      : natural := 8;
			G_COUNTER_SIZE       : natural := 8
		);
		port(
			clk        : in  std_logic;
			rst        : in  std_logic;
			enable     : in  std_logic;
			load       : in  std_logic;
			msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
			msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
			dout       : out std_logic_vector(31 downto 0);
            ack        : out std_logic;
            last_word  : out std_logic;
            last_bytes : out std_logic_vector(1 downto 0);
			ready      : out std_logic;
			done       : out std_logic
		);
	end component;

    -- types
    type chunk is array (0 to 15) of std_logic_vector(31 downto 0);
    
	-- signals 
	signal s_clk        : std_logic := '0';
	signal s_rst        : std_logic := '0';
	signal s_enable     : std_logic := '0';
	signal s_load       : std_logic := '0';
	signal s_dout       : std_logic_vector(31 downto 0);
    signal s_ack        : std_logic := '0';
	signal s_ready      : std_logic := '0';
    signal s_last_word  : std_logic := '0';
    signal s_last_bytes : std_logic_vector(1 downto 0) := "00";
	signal s_done       : std_logic := '0';
	
	for keygen_object : keygen use entity work.keygen(keygen_arch);

begin
	keygen_object : keygen 
		generic map (
			G_KEYGEN_PREFIX_SIZE => C_KEYGEN_PREFIX_SIZE,
			G_KEYGEN_SUFFIX_SIZE => C_KEYGEN_SUFFIX_SIZE,
			G_COUNTER_LIMIT      => C_COUNTER_LIMIT,  
			G_COUNTER_ORDER      => C_COUNTER_ORDER,
			G_COUNTER_SIZE       => C_COUNTER_SIZE
		)
		port map (
			s_clk, 
            s_rst, 
            s_enable,
            s_load,  
            C_KEYGEN_PREFIX, 
            C_KEYGEN_SUFFIX, 
            s_dout, 
            s_ack,
            s_last_word, 
            s_last_bytes,
            s_ready, 
            s_done
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

    enable_process : process(s_clk)
    begin
        if rising_edge(s_clk) then
        	if s_ready = '1' then
                s_enable <= '1';
			else 
                s_enable <= '0';
			end if;
        end if;
    end process enable_process;
    
    load_process : process(s_clk)
    begin
        if rising_edge(s_clk) then
        	if s_enable = '1' or s_ack = '1' then
                s_load <= '1';
			else 
                s_load <= '0';
			end if;
        end if;
    end process load_process;
    
	sim_process : process
	begin

		wait until s_done = '1';

	end process sim_process;
end keygen_test_arch;