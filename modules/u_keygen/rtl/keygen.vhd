library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity keygen is
	generic(
		G_KEYGEN_PREFIX_SIZE : natural := 12;  -- size in bits
		G_KEYGEN_SUFFIX_SIZE : natural := 12;  -- size in bits
		G_COUNTER_LIMIT      : std_logic_vector := "0001111";  
		G_COUNTER_ORDER      : natural := 8;
		G_COUNTER_SIZE       : natural := 8
	);
	port(
		clk        : in  std_logic;
		rst        : in  std_logic;
		enable     : in  std_logic;
		load       : in  std_logic;
		msg_prefix : in  std_logic_vector(G_KEYGEN_PREFIX_SIZE - 1 downto 0);
		msg_suffix : in  std_logic_vector(G_KEYGEN_SUFFIX_SIZE - 1 downto 0);
		dout       : out std_logic_vector(31 downto 0); 
		ack        : out std_logic;
		last_word  : out std_logic;
		last_bytes : out std_logic_vector(1 downto 0);
		ready      : out std_logic;
		done       : out std_logic
	);
end entity keygen;

architecture keygen_arch of keygen is
	-- constants
	constant C_MSG_COUNTER_CHAIN_SIZE : natural := G_COUNTER_ORDER * G_COUNTER_SIZE;
	constant C_MSG_SIZE_BITS  : natural := G_KEYGEN_PREFIX_SIZE + C_MSG_COUNTER_CHAIN_SIZE + G_KEYGEN_SUFFIX_SIZE;
	constant C_MSG_SIZE_WORDS : natural := C_MSG_SIZE_BITS / 32;
	constant C_MSG_LAST_BITS : natural := C_MSG_SIZE_BITS mod 32;
	constant C_MSG_LAST_BYTES : natural := C_MSG_LAST_BITS / 8;

	-- components
	component xbit_counter is
		generic(
			G_COUNTER_LIMIT : std_logic_vector := "0001111";
			G_COUNTER_SIZE  : natural := 8
		);
		port(
			clk         : in  std_logic;
			rst         : in  std_logic;
			enable      : in  std_logic;
			count       : in  std_logic;
			dout        : out std_logic_vector(G_COUNTER_SIZE - 1 downto 0);
			max_reached : out std_logic
		);
	end component;
	
	-- types
	type state_type is (C_IDLE, C_GENERATE_KEY, C_SERIALIZE_KEY, C_DONE);
	type chunk_type is array (0 to 15) of std_logic_vector(31 downto 0);
	
	-- signals
	signal vcount       : std_logic_vector(G_COUNTER_ORDER downto 0) := (others => '0');
	signal msg_counter  : std_logic_vector(C_MSG_COUNTER_CHAIN_SIZE - 1 downto 0) := (others => '0');
	signal msg          : std_logic_vector(0 to C_MSG_SIZE_BITS - 1);
	signal k            : natural range 0 to 15;  -- counts the number of words to be sent
	signal enable_q     : std_logic := '0';
	signal enable_pulse : std_logic := '0';
	signal serialize    : std_logic := '0';
	signal serialize_q  : std_logic := '0';
	signal state        : state_type := C_IDLE;
	
begin
	-- combinatorial
	msg          <= msg_prefix & msg_counter & msg_suffix;
	enable_pulse <= enable and (not enable_q);
	vcount(0)    <= enable_pulse;  -- enable on first counter

	-- generate chain of counters
	-- each counter will trigger a signal to the 
	-- n + 1 counter when it has reached its max
	CNT : for i in 0 to G_COUNTER_ORDER - 1 generate
	begin
		xbit_counter_object : xbit_counter 
			generic map (
				G_COUNTER_LIMIT => G_COUNTER_LIMIT, 
				G_COUNTER_SIZE  => G_COUNTER_SIZE
			)
			port map (
				clk, 
				rst, 
				enable,
				vcount(i), 
				msg_counter((i + 1) * G_COUNTER_SIZE - 1 downto i * G_COUNTER_SIZE),
				vcount(i + 1)
			);
	end generate;
	
	keygen_process : process(clk, rst)
	begin
		if rst = '0' then
			k           <= 0;
			ack         <= '0';
			dout        <= (others => '0');
			last_word   <= '0';
			last_bytes  <= (others => '0');
			ready       <= '0';
			done        <= '0';
			enable_q    <= '0';
			serialize_q <= '0';
			serialize   <= '0';
		elsif rising_edge(clk) then
			-- pulse detector delay : we need it 
			-- because we want to make sure that we 
			-- won't generate too many keys
			enable_q <= enable;

			-- latch serialize expression : we need it 
			-- because we need the serialize signal to be 
			-- high at same time as the load signal 
			if serialize = '1' then
				serialize_q <= serialize;
			end if;

			-- state machine starts here
			case state is
			when C_IDLE =>
				ack       <= '0';
				ready     <= '1';
				serialize <= '0';
				if enable_pulse = '1' then
					-- if the last counter has reached its 
					-- max, then we're done
					if vcount(G_COUNTER_ORDER) = '1' then 
						done      <= '1';
						state     <= C_DONE;
						serialize <= '0';
					else
						done      <= '0';
						state     <= C_GENERATE_KEY;
						serialize <= '1';
						ready     <= '0';
					end if;
				else
					state <= C_IDLE;
				end if;

			when C_GENERATE_KEY =>
				-- if the keygen is enabled, then we need
				-- to serialize the generated digest
				-- if the last counter's max_reached 
				-- is high, then we're done with the 
				-- digests generation, meaning that 
				-- we generated all of the possible 
				-- combinations
				-- if any of the counting signals is 
				-- high, then we need to serialize the 
				-- current generated digest
				serialize <= or_reduce(vcount);
				if serialize = '1' then 
					state <= C_SERIALIZE_KEY;
				end if;

			when C_SERIALIZE_KEY =>
				if serialize_q = '1' and load = '1' then
					if 0 <= k and k < C_MSG_SIZE_WORDS - 1 then
						last_word <= '0';
						dout      <= msg(k * 32 to (k + 1) * 32 - 1);
						ack       <= '1';
						last_word <= '0';
						k         <= k + 1;
						ready     <= '0';
					elsif k = C_MSG_SIZE_WORDS - 1 then 
						-- last word
						dout      <= msg(k * 32 to (k + 1) * 32 - 1);
						ack       <= '1';

						-- if bytes remain to be sent
						if 0 < C_MSG_LAST_BYTES then
							k     <= k + 1;
							ready <= '0';
						else
							k     <= 0;
							last_word <= '1';
							ready <= '1';
							state <= C_IDLE;
						end if;
					elsif k = C_MSG_SIZE_WORDS then 
						-- remaining bytes
						case C_MSG_LAST_BYTES is 
						when 1 =>  -- 1 byte to load
							dout( 7 downto  0) <= msg(C_MSG_SIZE_BITS -  8 to C_MSG_SIZE_BITS - 1);
							dout(31 downto  8) <= (others => '0');
							ack                <= '1';
						when 2 =>  -- 2 bytes to load
							dout(15 downto  0) <= msg(C_MSG_SIZE_BITS - 16 to C_MSG_SIZE_BITS - 1);
							dout(31 downto 16) <= (others => '0');
							ack                <= '1';
						when 3 =>  -- 3 bytes to load
							dout(23 downto  0) <= msg(C_MSG_SIZE_BITS - 24 to C_MSG_SIZE_BITS - 1);
							dout(31 downto 24) <= (others => '0');
							ack                <= '1';
						when others => 
						end case;
						
						last_word <= '1';
						k          <= 0;
						ready      <= '1';
						last_bytes <= std_logic_vector(to_unsigned(C_MSG_LAST_BYTES, 2));
						state      <= C_IDLE;  -- we're done
					else 
						-- something went wrong...
						ack   <= '0';
						k     <= 0;
						ready <= '0';
						state <= C_IDLE;
					end if;
				else 
					ack   <= '0';
					ready <= '0';
				end if;

			when C_DONE =>
			when others =>
			end case; 
		end if;
	end process keygen_process;
end keygen_arch;
