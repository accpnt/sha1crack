library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity xbit_counter is
    generic(
        -- counter size
        G_COUNTER_LIMIT : std_logic_vector := "0001111";
		G_COUNTER_SIZE  : natural := 8
	);
	port(
		clk         : in  std_logic;
		rst         : in  std_logic;
		enable      : in  std_logic;
		count       : in  std_logic;
		dout        : out std_logic_vector(G_COUNTER_SIZE - 1 downto 0);
		max_reached : out std_logic
	);
end entity xbit_counter;

-- normal xbit reverse counter (counter span from 0 to 2^x - 1)
architecture xbit_counter_arch of xbit_counter is
begin
	xbit_counter_process : process(clk, rst)
		variable cnt : std_logic_vector(G_COUNTER_SIZE - 1 downto 0) := (others => '0');
	begin
		if rst = '0' then
			cnt         := (others => '0');
			dout        <= (others => '0');
			max_reached <= '0';
		elsif rising_edge(clk) then
			if enable = '1' then
				if count = '1' then 
					if cnt = G_COUNTER_LIMIT then
						cnt         := (others => '0');
						max_reached <= '1';
					else
						cnt         := cnt + 1;
						max_reached <= '0';
					end if;
				else
					max_reached <= '0';
				end if;
			end if;
		end if;	
		dout <= cnt;
	end process xbit_counter_process;
end xbit_counter_arch;
