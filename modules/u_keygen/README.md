# u_keygen

## Overview 

This keygen parses a user defined keyspace using xbit reverse counters (counter span from 0 to 2^x - 1)

## Design

The keygen is just a set of chained xbit reverse counters. 