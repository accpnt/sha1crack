#!/bin/sh

# create library
vlib work
vmap work work

# compile RTL files
vcom -93 -work work ../rtl/xbit_counter.vhd
vcom -93 -work work ../rtl/keygen.vhd

# compile testbench
vcom -93 -work work ../tst/keygen_test.vhd

# launch ModelSim 
vsim -t 1ps keygen_test -do ./keygen.do

