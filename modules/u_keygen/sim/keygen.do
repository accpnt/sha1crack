onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /keygen_test/keygen_object/clk
add wave -noupdate /keygen_test/keygen_object/rst
add wave -noupdate /keygen_test/keygen_object/enable
add wave -noupdate /keygen_test/keygen_object/load
add wave -noupdate -radix hexadecimal /keygen_test/keygen_object/msg_prefix
add wave -noupdate -radix hexadecimal /keygen_test/keygen_object/msg_suffix
add wave -noupdate -radix hexadecimal /keygen_test/keygen_object/dout
add wave -noupdate /keygen_test/keygen_object/ack
add wave -noupdate /keygen_test/keygen_object/last_word
add wave -noupdate /keygen_test/keygen_object/last_bytes
add wave -noupdate /keygen_test/keygen_object/ready
add wave -noupdate /keygen_test/keygen_object/done
add wave -noupdate /keygen_test/keygen_object/vcount
add wave -noupdate -radix hexadecimal /keygen_test/keygen_object/msg_counter
add wave -noupdate -radix hexadecimal /keygen_test/keygen_object/msg
add wave -noupdate /keygen_test/keygen_object/k
add wave -noupdate /keygen_test/keygen_object/enable_q
add wave -noupdate /keygen_test/keygen_object/enable_pulse
add wave -noupdate /keygen_test/keygen_object/serialize
add wave -noupdate /keygen_test/keygen_object/serialize_q
add wave -noupdate /keygen_test/keygen_object/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {35957587 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1350167 ps}
