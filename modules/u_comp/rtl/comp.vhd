library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity comp is
    generic(
		G_CRACK_HASH : std_logic_vector(159 downto 0)
	);
	port(
		clk    : in  std_logic;
		rst    : in  std_logic;
		enable : in  std_logic;
		din    : in  std_logic_vector(159 downto 0);
		result : out std_logic;
		ready  : out std_logic
	);
end entity comp;

-- normal xbit reverse counter (counter span from 0 to 2^x - 1)
architecture comp_arch of comp is
begin
	comp_process : process(clk, rst)
	begin
		if rst = '0' then
			ready  <= '0';
			result <= '0';
		elsif rising_edge(clk) then
			ready <= '0';
			if enable = '1' then
				if din = G_CRACK_HASH then 
					result <= '1';
				else
					result <= '0';
				end if;
				ready <= '1';
			else
				ready <= '0';
			end if;
		end if;	
	end process comp_process;
end comp_arch;
