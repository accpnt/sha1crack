library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity comp_test is
end entity comp_test;

architecture comp_test_arch of comp_test is
	-- constants
	constant C_CRACK_HASH : std_logic_vector := X"32d10c7b8cf96570ca04ce37f2a19d84240d3a89";

	-- components
	component comp
		generic(
			G_CRACK_HASH : std_logic_vector(159 downto 0)
		);
		port(
			clk    : in  std_logic;
			rst    : in  std_logic;
			enable : in  std_logic;
			din    : in  std_logic_vector(159 downto 0);
			result : out std_logic;
			ready  : out std_logic
		);
	end component;

	-- signals
	signal s_clk    : std_logic := '0';
	signal s_rst    : std_logic := '0';
	signal s_enable : std_logic := '0';
	signal s_din    : std_logic_vector(159 downto 0) := (others => '0');
	signal s_result : std_logic := '0';
	signal s_ready  : std_logic := '0';
	
	for comp_object : comp use entity work.comp(comp_arch);

begin
	comp_object : comp 
		generic map (G_CRACK_HASH => C_CRACK_HASH)
		port map (
			s_clk, s_rst, s_enable, s_din, s_result, s_ready
		);

	rst_process : process
	begin
		s_rst <= '0';
		wait for 100 ns;
		s_rst <= '1';
		wait;
	end process rst_process;

	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;

	sim_process : process
	begin
		wait for 100 ns;
		s_din    <= X"32d10c7b8cf96570ca04ce37f2a19d84240d3a89";
		s_enable <= '1';
		
		wait for 150 ns;
		s_enable <= '0';
		
		wait for 100 ns;
		s_din    <= X"32d10c7b8cf96570ca04ce37f2a19d84240d3a88";
		s_enable <= '1';

		wait;
        
	end process sim_process;
end comp_test_arch;
