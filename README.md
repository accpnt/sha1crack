# sha1crack

A SHA-1 bruteforce cracker for SL3 hash written in VHDL.


## Overview

The sha1crack system is a FPGA-based SHA-1 bruteforce cracker. Its purpose is to decrypt a SL3 Nokia hash in order to retrieve the NCK code. 

## Design
The design is composed of a key generator, a SHA-1 encryption engine and a comparator module. 

![Link to the design architecture diagram](https://gitlab.com/accpnt/sha1crack/raw/master/modules/s_crack/doc/s_crack.svg)


The key generator searches the full [0:9] keyspace. Each generated key is encrypted with the SHA-1 engine. The hash output is then compared to the hash we want to decrypt using the comparator module. 

The key search operation is pipelined. If N is the pipeline depth, then we have : 

- 1 instance of the key generator that generates N keys to be encrypted
- N instances of the SHA-1 engine 
- N instances of the comparator module.

## Implementation
The design is coded in VHDL.
  
### Modules

* Top
    * t_fpga : the top core module is project specific. 
* Stage
    * s_crack : the main stage module where all the unit modules are instanciated 
* Unit
    * u_sha1 : the SHA-1 engine module
    * u_keygen : the key generator
    * u_comp : the hash comparator module

#### u_keygen

The keygen is a chain of counters. All the counters trigger a signal when they reach their max value. This signal is connected to the N+1 counting input port. The counting output of each counter is concatenated. It's also possible to add a prefix and a suffix to the counter generated vector. The resulting digest is then serialized into 32 bits wide vectors. 

#### u_sha1

The SHA-1 engine is divided in two parts : a preprocessor and a computing unit. 

### Project

The project folder contains a minimal set of Quartus files that will generate the programming files. The output is intended to be run on a DE0-NANO board. 

### Configuration

The system is configured in [sha1crack/modules/t_fpga/rtl/fpga.vhd](https://gitlab.com/accpnt/sha1crack/blob/master/modules/t_fpga/rtl/fpga.vhd) using the following VHDL generics : 

| Constants            | Type             | Purpose                                                        |
|----------------------|------------------|----------------------------------------------------------------|
| C_CRACK_PIPELINE     |      natural     | The key search pipeline depth                                  |
| C_CRACK_HASH         | std_logic_vector | The hash we want to decrypt. Must be 160 bits wide (mandatory) |
| C_KEYGEN_PREFIX      | std_logic_vector | Append a prefix to the generated keys (not mandatory)          |
| C_KEYGEN_PREFIX_SIZE |      natural     | Prefix size                                                    |
| C_KEYGEN_SUFFIX      | std_logic_vector | Append a suffix to the generated keys (not mandatory)          |
| C_KEYGEN_SUFFIX_SIZE |      natural     | Suffix size                                                    |
| C_COUNTER_ORDER      |      natural     | The number of counters to be chained                           |
| C_COUNTER_SIZE       |      natural     | Length in bits of the keygen counters                          |
| C_COUNTER_LIMIT      | std_logic_vector | Defines the counter maximum value                              |

### Interfaces

The system does not provide any interfaces. It is intended to run along with SignalTap. The key search output should be displayed when the Top core ready signal rises to high. 

## Example

## Links
[NSA@home](http://nsa.unaligned.org) is a fast FPGA-based SHA-1 and MD5 bruteforce cracker written in Verilog. 
